#!/bin/bash

certbot certonly \
--dns-route53 \
--dns-route53-propagation-seconds 30 \
-d back-end-dev-3.mydomain.com \
-m me@me.com \
--logs-dir /home/user/letsencrypt/logs \
--config-dir /home/user/letsencrypt/config \
--work-dir /home/user/letsencrypt/work \
--agree-tos \
--non-interactive